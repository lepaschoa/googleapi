﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Descrição resumida de GeoCodeResult
/// </summary>
public class GeoCodeResult
{
    public string status { get; set; }
    public results[] results { get; set; }
}