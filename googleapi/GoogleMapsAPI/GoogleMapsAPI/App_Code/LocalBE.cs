﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Descrição resumida de LocalBE
/// </summary>
public class LocalBE
{
    public string Latitude { get; set; }
    public string Logitude { get; set; }
    public string Endereco { get; set; }
}