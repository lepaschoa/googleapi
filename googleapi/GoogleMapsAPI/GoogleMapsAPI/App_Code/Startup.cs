﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(GoogleAboutAPI.Startup))]
namespace GoogleAboutAPI
{
    public partial class Startup {
        public void Configuration(IAppBuilder app) {
            ConfigureAuth(app);
        }
    }
}
