﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="_Default" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">

    <style>
        .Grid {
    background-color: #fff;
    margin: 5px 0 10px 0;
    border: solid 1px #525252;
    border-collapse: collapse;
    font-family: Calibri;
    color: #474747;
}

    .Grid td {
        padding: 2px;
        border: solid 1px #c1c1c1;
    }

    .Grid th {
        padding: 4px 2px;
        color: #fff;
        background: #363670 url(Images/grid-header.png) repeat-x top;
        border-left: solid 1px #525252;
        font-size: 0.9em;
    }

    .Grid .alt {
        background: #fcfcfc url(Images/grid-alt.png) repeat-x top;
    }

    .Grid .pgr {
        background: #363670 url(Images/grid-pgr.png) repeat-x top;
    }

        .Grid .pgr table {
            margin: 3px 0;
        }

        .Grid .pgr td {
            border-width: 0;
            padding: 0 6px;
            border-left: solid 1px #666;
            font-weight: bold;
            color: #fff;
            line-height: 12px;
        }

        .Grid .pgr a {
            color: Gray;
            text-decoration: none;
        }

            .Grid .pgr a:hover {
                color: #000;
                text-decoration: none;
            }
    </style>

    <div>        
        <asp:TextBox runat="server" ID="busca" CssClass="divBusca" />
        <asp:Button runat="server" Text="Buscar" OnClick="BuscarEnd" CssClass="buttonBusca" />
   </div>

    <div id="googleMap" style="width:100%;height:610px;"></div>

    <script>
        function myMap() {

            var mapCanvas = document.getElementById("googleMap");
            var myCenter = new google.maps.LatLng(51.508742, -0.120850);
            var mapOptions = { center: myCenter, zoom: 5 };
            var map = new google.maps.Map(mapCanvas, mapOptions);
            google.maps.event.addListener(map, 'click', function (event) {
                placeMarker(map, event.latLng);
            });          
        }

        function placeMarker(map, location) {
            var marker = new google.maps.Marker({
                position: location,
                map: map
            });
            var infowindow = new google.maps.InfoWindow({
                content: 'Latitude: ' + location.lat()
                + '<br>Longitude: ' + location.lng()
                + '<br> <asp:Button runat="server" Text="Add" OnClick="AddLocal" CssClass="buttonBusca" />'
            });
            infowindow.open(map, marker);
        }

    </script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA-1wXUYnDVaaM2YutYa-eovXFc6hEQY-k&callback=myMap"
    async defer></script>

    <div style="margin-top: 5%">
        <asp:GridView ID="idGrid" runat="server" AutoGenerateColumns="false" CSSClass="tabulardata" CellSpacing="1" >
        <Columns>	        

            <asp:BoundField DataField="Latitude" HeaderText="Latitude" />
            <asp:BoundField DataField="Logitude" HeaderText="Logitude" />
            <asp:BoundField DataField="Endereco" HeaderText="Endereço Completo" />
            	     
        </Columns> 
    </asp:GridView>
    </div>
    
</asp:Content>
