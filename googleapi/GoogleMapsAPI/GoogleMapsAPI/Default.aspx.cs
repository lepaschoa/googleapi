﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class _Default : Page
{
    #region VariaveisGlobais
    string apiKey = "AIzaSyA-1wXUYnDVaaM2YutYa-eovXFc6hEQY-k";
    List<LocalBE> ListaLugares = new List<LocalBE>();
    #endregion


    protected void Page_Load(object sender, EventArgs e)
    {
    }

    protected void BuscarEnd(object sender, EventArgs e)
    {
        var valor = busca.Text;
        BuscaEndereco(valor);
    }

    protected void AddLocal(object sender, EventArgs e)
    {
        
    }

    private void BuscaEndereco(string valor)
    {
        valor = valor.Replace(" ", "+");
        var url = string.Format("https://maps.googleapis.com/maps/api/place/textsearch/json?query={0}&key={1}", valor, apiKey);

        var request = WebRequest.Create(url);
        request.ContentType = "application/json; charset=utf-8";

        string retornoJson = string.Empty;

        var response = (HttpWebResponse)request.GetResponse();

        using(var sr = new StreamReader(response.GetResponseStream()))
        {
            retornoJson = sr.ReadToEnd();

        }

        var jss = new JavaScriptSerializer();

        GeoCodeResult gl = jss.Deserialize<GeoCodeResult>(retornoJson);
        GeoLocalizacao(gl);
    }

    private void GeoLocalizacao(GeoCodeResult gl)
    {   
        if (gl.status == "OK")
        {
            LocalBE local = new LocalBE();
            local.Latitude = gl.results[0].geometry.location.lat;
            local.Logitude = gl.results[0].geometry.location.lng;
            local.Endereco = gl.results[0].formatted_address;

            ListaLugares.Add(local);

            CarregarLugares();
        }
    }

    void CarregarLugares()
    {
        idGrid.DataSource = ListaLugares;
        idGrid.DataBind();  //mostra dados na tela
    }
}